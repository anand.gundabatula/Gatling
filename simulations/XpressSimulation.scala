
import scala.concurrent.duration._
import XpressRequests._
import utilities._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class XpressSimulation extends Simulation {

      val finalvalue = RndNumber.rndItgr
      println(finalvalue)
      
      val _data01_feeder = csv("CustInqRq.csv").circular
      
      val httpProtocol = http
		.baseURL("http://10.0.50.21:9083")
		.inferHtmlResources()
		.acceptEncodingHeader("gzip,deflate")
		.contentTypeHeader("text/xml")
		.userAgentHeader("Apache-HttpClient/4.1.1 (java 1.5)")

    	val uri1 = "10.0.50.21"
    	val scn = scenario("XpressSimulation")
    		.feed(_data01_feeder)
		.exec(http("request_0")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(ib01AcctInqRq.sendrequest)))
			
	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)

}


