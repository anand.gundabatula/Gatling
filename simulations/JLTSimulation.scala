import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class JLTSimulation extends Simulation {

	val httpProtocol = http
		.baseURL("http://10.0.48.223:8123")
		.inferHtmlResources()
		.acceptHeader("text/html, application/xhtml+xml, */*")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-US")
		.doNotTrackHeader("1")
		.userAgentHeader("Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)")

	val _data02_feeder=csv("bp02.csv").random
	
	val bp02scn = scenario("ATMCashWithdrawal")
		.feed(_data02_feeder)
		.during(90 minutes) 
		{ 
		   exec(
		   	http("ATMCashWithdrawal")
			.get("/bp?bpid=VTB_ATMCashWithdrawal&Account=${acct_num}&CurDate=0901")
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(40 seconds)
		}

	val _data01_feeder = csv("bp01.csv").random
	
	val bp01scn = scenario("ATMBalInq")
		.feed(_data01_feeder)
		.during(90 minutes) 
		{ 
		   exec(
		   	http("ATMBalInq")
			.get("/bp?bpid=VTB_ATMBalInq&Account=${acct_num}&CurDate=0901")
			.check(regex("FAILED").notExists)
			).pause(40 seconds)
		}

	val _data03_feeder = csv("bp03.csv").random
	val bp03scn = scenario("ATMInternalFT")
		.feed(_data03_feeder)
		.during(90 minutes) 
		{ 
		   exec(
		   	http("ATMInternalAccountFT")
			.get("/bp?bpid=VTB_ATMInternalFT&CurDate=0901&BRCD=${branchcode}&Account=${fromAcct}&OtherAccount=${toAcct}")
			.check(regex("FAILED").notExists)
			).pause(40 seconds)
		}

	val _data04_feeder = csv("bp03.csv").random
	val bp04scn = scenario("ATMInternalCardFT")
		.feed(_data04_feeder)
		.during(90 minutes) 
		{ 
		   exec(
		   	http("ATMInternalCardFT")
			.get("/bp?bpid=VTB_ATMInternalCFT&CurDate=0901&BRCD=${branchcode}&Account=${fromAcct}&OtherAccount=${toAcct}")
			.check(regex("FAILED").notExists)
			).pause(40 seconds)
		}

	setUp(
	  bp01scn.inject(
	    nothingFor(1 seconds),
	    atOnceUsers(3),
	    rampUsers(525) over(44 minutes) //528 [12 users in 1min(2 users every 5 secs), so 12*44mins = 528]	    
	   ).protocols(httpProtocol),
	    
	  bp02scn.inject(
	    nothingFor(1 seconds),
	    atOnceUsers(3),
    	    rampUsers(3390) over(90 minutes) //3383 [12*90 mins = 1080]   
	  ).protocols(httpProtocol),

	  bp03scn.inject(
	    nothingFor(1 seconds),
	    atOnceUsers(3),
    	    rampUsers(261) over(22 minutes) //264  [12 * 22mins = 264]  
	  ).protocols(httpProtocol),
	  
	  bp04scn.inject(
	    nothingFor(1 seconds),
	    atOnceUsers(3),
    	    rampUsers(217) over(4 minutes) //220 [12*19mins = 228]
	  ).protocols(httpProtocol)
	  
  	)
}

