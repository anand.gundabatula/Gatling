
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._
import utilities._

class XpressTest extends Simulation {
	
      
      // IB01 - TestData Input Files
      val _ib01_acctinqrq_data_feeder = csv("ib01AcctInqRq.csv").random
      val _client_datetime = csv("client_datetime.csv").random
      val _session_key = csv("session_key.csv").random
      val _ib01_acctpromorq_data_feeder = csv("ib01AccountPromoInqRq.csv").random
      val _ib01_custrq_data_feeder = csv("ib01CustInqRq.csv").random
      val _ib01_inboxalertrq_data_feeder = csv("ib01InboxAlertInqRq.csv").random
      
      // IB02 - TestData Input Files
      val _ib02_feerq_data_feeder = csv("ib02FeeInqRq.csv").random      
      val _ib02_pmtacct1_data_feeder = csv("ib02PmtAcct.csv").random
      val _ib02_pmtacct2_data_feeder = csv("ib02PmtAcct2.csv").random
      
      // IB03 - TestData Input Files
      val _ib03_balinq_data_feeder = csv("ib03BalInq.csv").random
      val _ib03_xferadd_data_feeder = csv("ib03xferadd.csv").random 
      
      //IB04 - Bill Payment
      val _ib03_AutoBillRegInqRq_data_feeder = csv("ib03AutoBillRegInqRq.csv").random
      
      //Random Number generation
      val finalRndNbr = RndNumber.rndItgr
      //println(finalRndNbr)
      
      def IB01AcctInqRq = """<IFX xmlns="http://www.ifxforum.org/IFX_150">
			<SignonRq>
				<SessKey>${session_key}</SessKey>
				<ClientDt>${client_datetime}.000000-00:00</ClientDt>
				<CustLangPref>en_US</CustLangPref>
				<ClientApp>
					<Org>com.fidelity</Org>
					<Name>Fidelity</Name>
					<Version>1.0</Version>
				</ClientApp>
			</SignonRq>
			<BankSvcRq>
				<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</RqUID>
				<AsyncRqUID>00000000-0000-0000-0000-000000000000</AsyncRqUID>
				<SPName>com.fnf.xes.PRF</SPName>
				<ns1:AcctInqRq xmlns:ns1="http://www.fnf.com/xes/services/acct/acctinq/v2_1">
					<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</RqUID>
					<DepAcctId>
						<AcctId>${dda_account}</AcctId>
						<AcctType>DDA</AcctType>
						<BankInfo></BankInfo>
					</DepAcctId>
				</ns1:AcctInqRq>
			</BankSvcRq>
		</IFX>"""
      
       def IB01AcctPromoInqRq = """<IFX xmlns="http://www.ifxforum.org/IFX_150" xmlns:com.fnf="http://www.fnf.com/xes" xmlns:com.ctg="http://www.fnf.com/xes/ctg">
			<SignonRq>
				<SessKey>${session_key}</SessKey>
				<ClientDt>${client_datetime}.000641-04:00</ClientDt>
				<CustLangPref>en_US</CustLangPref>
				<ClientApp>
					<Org>com.fnf</Org>
					<Name>Fidelity Bank</Name>
					<Version>1.0</Version>
				</ClientApp>
				<SuppressEcho>0</SuppressEcho>
			</SignonRq>
			<BankSvcRq Id="ID000001">
				<RqUID>0a790d2a-0170-a0d3-0137-0f17cca48002</RqUID>
				<SPName>com.fnf.xes.PRF</SPName>
				<com.ctg:AccountPromoInqRq>
					<RqUID>0a790d2a-0170-a0d3-0137-0f17cca48002</RqUID>
					<AcctId>${promo_acct}</AcctId>
				</com.ctg:AccountPromoInqRq>
			</BankSvcRq>
		</IFX>"""

      def IB01CustInqRq = """<IFX xmlns="http://www.ifxforum.org/IFX_150">
			<SignonRq>
				<SessKey>${session_key}</SessKey>
				<ClientDt>${client_datetime}.000412+07:00</ClientDt>
				<CustLangPref>en_US</CustLangPref>
				<ClientApp>
					<Org>com.fnf</Org>
					<Name>A112_TPSS</Name>
					<Version>1.0</Version>
				</ClientApp>
				<SuppressEcho>0</SuppressEcho>
			</SignonRq>
			<BaseSvcRq Id="ID000001">
				<RqUID>0a068822-51ce-36be-014f-4407d7a28c71</RqUID>
				<SPName>com.fnf.xes.PRF</SPName>
				<ns1:CustInqRq xmlns:ns1="http://www.fnf.com/xes/ctg/services/cust/custinq/v2_3">
					<RqUID>0a068822-51ce-36be-014f-4407d7a28c72</RqUID>
					<CustId>
						<SPName>com.fnf.xes.PRF</SPName>
						<CustPermId>${cust_nbr}</CustPermId>
					</CustId>
				</ns1:CustInqRq>
			</BaseSvcRq>
		</IFX>"""
      
      	def IB01InboxAlertInqRq = """<IFX xmlns="http://www.ifxforum.org/IFX_150">	
			<SignonRq>
				<SessKey>${session_key}</SessKey>
				<ClientDt>${client_datetime}.000000-00:00</ClientDt>
				<CustLangPref>en_US</CustLangPref>
				<ClientApp>
					<Org>com.fnf</Org>
					<Name>FNF Bank</Name>
					<Version>1.0</Version>
				</ClientApp>
			</SignonRq>
			<BaseSvcRq Id="ID000001">
				<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b473</RqUID>
				<AsyncRqUID>00000000-0000-0000-0000-000000000000</AsyncRqUID>
				<SPName>com.fnf.xes.PRF</SPName>
				<com.ctg:InboxAlertInqRq xmlns:com.ctg="http://www.fnf.com/xes/ctg">
					<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b473</RqUID>
					<RefInfo>
						<RefType>IB</RefType>
						<RefId>1</RefId>
					</RefInfo>
					<com.ctg:InboxAlertInfo>
						<CustId>
							<SPName>com.fnf.xes.PRF</SPName>
							<CustPermId>${inboxalert_cust_nbr}</CustPermId>
						</CustId>
						<StatusDesc>Sent</StatusDesc>
					</com.ctg:InboxAlertInfo>
				</com.ctg:InboxAlertInqRq>
			</BaseSvcRq>
		</IFX>"""
      	
	def IB02FeeInqRq = """<IFX xmlns="http://www.ifxforum.org/IFX_150">
				<SignonRq>
					<SessKey>${session_key}</SessKey>
					<ClientDt>${client_datetime}.000000-00:00</ClientDt>
					<CustLangPref>en_US</CustLangPref>
					<ClientApp>
						<Org>com.fidelity</Org>
						<Name>A101_IBR</Name>
						<Version>1.0</Version>
					</ClientApp>
				</SignonRq>
				<BaseSvcRq>
					<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</RqUID>
					<AsyncRqUID>00000000-0000-0000-0000-000000000000</AsyncRqUID>
					<SPName>com.fnf.xes.PRF</SPName>
					<ns1:FeeInqRq xmlns:ns1="http://www.fnf.com/xes/ctg">
						<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</RqUID>
						<RefInfo>
							<RefType>A101_IBR</RefType>
							<RefId>58033</RefId>
						</RefInfo>
						<ns1:FeeInqInfo>"
							<ProductId></ProductId>
							<FeeType></FeeType>
							<BranchId>${branch_id}</BranchId>
							<PmtType>DRO</PmtType>
							<PmtMethod>2</PmtMethod>
							<ns1:ServiceProviderName></ns1:ServiceProviderName>
							<CustType>0</CustType>
							<AcctId>${feeInq_acct_ID}</AcctId>
							<ns1:TrnAmt>
								<Amt>${feeinqamt}</Amt>
								<CurCode>VND</CurCode>
							</ns1:TrnAmt>
						</ns1:FeeInqInfo>
					</ns1:FeeInqRq>
				</BaseSvcRq>
		</IFX>"""
      	
      		def IB02PmtAddRq = """<IFX xmlns="http://www.ifxforum.org/IFX_150">
				<SignonRq>
					<SessKey>${session_key}</SessKey>
					<ClientDt>${client_datetime}.000000-00:00</ClientDt>
					<CustLangPref>en_US</CustLangPref>
					<ClientApp>
						<Org>com.fidelity</Org>
						<Name>A101_IBR</Name>
						<Version>1.0</Version>
					</ClientApp>
				</SignonRq>
				<BankSvcRq>
					<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</RqUID>
					<AsyncRqUID>00000000-0000-0000-0000-000000000000</AsyncRqUID>
					<SPName>com.fnf.xes.PRF</SPName>
					<ns6:PmtAddRq xmlns:ns6="http://www.fnf.com/xes/ctg/services/pmt/pmtadd/v1_0" xmlns:ns1="http://www.fnf.com/xes/ctg" xmlns:ns2="http://www.fnf.com/xes">
						<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</RqUID>
						<AsyncRqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</AsyncRqUID>
						<RefInfo>
							<RefType>A101_IBR</RefType>
							<RefId>12312</RefId>
						</RefInfo>
						<ns2:UserId>1</ns2:UserId>
						<ns1:ServicingBranch>${pmt_branch}</ns1:ServicingBranch>
						<ns1:PmtInfo>
							<PmtType>Outgoing IBPS_Bilateral</PmtType>
							<PmtMethod>Account</PmtMethod>
							<TrnType>Regular transaction</TrnType>
						</ns1:PmtInfo>
						<ns1:FromPmtInfo>
							<DepAcctIdFrom>
								<AcctId>${from_pmt_acct}</AcctId>
								<AcctType></AcctType>
								<BankInfo></BankInfo>
							</DepAcctIdFrom>
							<ns1:PayerInfo>
								<PersonInfo>
									<NameAddrType></NameAddrType>
									<FullName></FullName>
								</PersonInfo>
								<BankInfo>
									<BranchId></BranchId>
								</BankInfo>
								<CustType></CustType>
							</ns1:PayerInfo>
						</ns1:FromPmtInfo>
						<ns1:ToPmtInfo>
							<DepAcctIdTo>
								<AcctId>${to_pay_acct}</AcctId>
								<AcctType></AcctType>
								<BankInfo>
									<BranchId></BranchId>
								</BankInfo>
							</DepAcctIdTo>
							<ns1:PayeeInfo>
								<BankInfo>
									<BankId>${brcd}</BankId>
									<BranchId>${key}</BranchId>
								</BankInfo>
								<ns3:BeneficiaryName xmlns:ns3="http://www.fnf.com/xes">DaaaHoiii</ns3:BeneficiaryName>
							</ns1:PayeeInfo>
						</ns1:ToPmtInfo>
						<ns1:TrnInfo>
							<ns5:TrnCode xmlns:ns5="http://www.fnf.com/xes">SW</ns5:TrnCode>
							<ns1:TrnAmt>
								<Amt>${pmt_amount}</Amt>
								<CurCode>VND</CurCode>
								<ns1:AmtType>TRAN_AMOUNT</ns1:AmtType>
							</ns1:TrnAmt>
							<ns2:TransDescription>3_11 DT</ns2:TransDescription>
							<EffDt>2015-09-01</EffDt>
							<ns2:Frequency>
								<Freq>1DA</Freq>
							</ns2:Frequency>
							<ns1:ChannelPmtType>EXP</ns1:ChannelPmtType>
						</ns1:TrnInfo>
						<ns1:FeeInfo>
							<FeeType>IB01</FeeType>
							<ns1:TrnAmt>
								<Amt>0.0</Amt>
								<ns1:AmtType>FEE_AMOUNT</ns1:AmtType>
							</ns1:TrnAmt>
							<ns1:TrnAmt>
								<Amt>0</Amt>
								<ns1:AmtType>TOTAL_FEE_AMOUNT</ns1:AmtType>
							</ns1:TrnAmt>
							<ns1:VATAmt>
								<Amt>0</Amt>
							</ns1:VATAmt>
							<ns1:FeeAmt>0</ns1:FeeAmt>
						</ns1:FeeInfo>
					</ns6:PmtAddRq>
				</BankSvcRq>
		</IFX>"""
      	
      		def IB03BalInqRq = """<IFX xmlns="http://www.ifxforum.org/IFX_150">
				<SignonRq>
					<SessKey>${session_key}</SessKey>
					<ClientDt>${client_datetime}.000000-00:00</ClientDt>
					<CustLangPref>en_US</CustLangPref>
					<ClientApp>
						<Org>com.fnf</Org>
						<Name>A112_TPSS</Name>
						<Version>1.0</Version>
					</ClientApp>
					<SuppressEcho>0</SuppressEcho>
				</SignonRq>
				<BankSvcRq Id="ID000001">
					<RqUID>0a068822-51ce-36be-014f-4407d7a28a4b</RqUID>
					<SPName>com.fnf.xes.PRF</SPName>
					<ns1:BalInqRq xmlns:ns1="http://www.fnf.com/xes/services/acctbalance/acctbalanceinq/v2_0">
						<RqUID>0a068822-51ce-36be-014f-4407d7a28a4c</RqUID>
						<DepAcctId>
							<AcctId>${balInq_acct_nbr}</AcctId>
							<AcctType>CD</AcctType>
							<BankInfo></BankInfo>
						</DepAcctId>
					</ns1:BalInqRq>
				</BankSvcRq>
			</IFX>"""

		def IB03XferAddRq = """<IFX xmlns:com.fnf="http://www.fnf.com/xes" xmlns:com.fnf.fundstransferadd_V2_0="http://www.fnf.com/xes/services/fundstransfer/fundstransferadd/v2_0" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:com.ctg="http://www.fnf.com/xes/ctg" xmlns="http://www.ifxforum.org/IFX_150" xmlns:ns2="http://www.fnf.com/xes">
			<SignonRq>
				<SessKey>${session_key}</SessKey>
				<ClientDt>${client_datetime}.000000-00:00</ClientDt>
				<CustLangPref>en_US</CustLangPref>
				<ClientApp>
					<Org>com.fnf</Org>
					<Name>A112_TPSS</Name>
					<Version>1.0</Version>
				</ClientApp>
			</SignonRq>
			<BankSvcRq>
				<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b473</RqUID>
				<SPName>com.fnf.xes.PRF</SPName>
				<com.fnf.fundstransferadd_V2_0:XferAddRq>
					<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b473</RqUID>
					<RefInfo>
						<RefType>A112_TPSS</RefType>
						<RefId>A27_TR_12345</RefId>
					</RefInfo>
					<com.fnf:XferInfo>
						<DepAcctIdFrom>
							<AcctId>${from_account_num}</AcctId>
							<AcctType></AcctType>
							<AcctCur></AcctCur>
							<BankInfo>
								<BranchId></BranchId>
							</BankInfo>
						</DepAcctIdFrom>
						<DepAcctIdTo>
							<AcctId>${to_account_num}</AcctId>
							<AcctType></AcctType>
							<BankInfo></BankInfo>
						</DepAcctIdTo>
						<CurAmt>
							<Amt>1</Amt>
						</CurAmt>
						<com.fnf:XferAdditionalInfo>
							<com.fnf:TellerAdditionalInfo>
								<com.fnf:ReversedInd>N</com.fnf:ReversedInd>
							</com.fnf:TellerAdditionalInfo>
							<com.ctg:FeeInfo>
								<FeeType>FIXED</FeeType>
								<com.ctg:TrnAmt>
									<Amt>0</Amt>
									<com.ctg:AmtType>FEE_AMOUNT</com.ctg:AmtType>
								</com.ctg:TrnAmt>
								<com.ctg:TrnAmt>
									<Amt>0</Amt>
									<com.ctg:AmtType>VAT_AMOUNT</com.ctg:AmtType>
								</com.ctg:TrnAmt>
								<DepAcctId>
									<AcctId>${from_account_num}</AcctId>
									<AcctType></AcctType>
									<BankInfo></BankInfo>
								</DepAcctId>
								<com.ctg:FeeIncludeFlg>N</com.ctg:FeeIncludeFlg>
								<com.ctg:FeeChargeOption>2</com.ctg:FeeChargeOption>
							</com.ctg:FeeInfo>
						</com.fnf:XferAdditionalInfo>
						<Memo>Transfer funds to secondary account</Memo>
						<ns2:PostingDt>2015-09-01</ns2:PostingDt>
						<com.ctg:TrnRefNum>${tran_ref_num}${finalRndNbr}</com.ctg:TrnRefNum>						
						<EffDt>2015-10-10</EffDt>
					</com.fnf:XferInfo>
					<TrnType></TrnType>
					<com.fnf:UserId>XES</com.fnf:UserId>
					<com.ctg:ServicingBranchId>${from_branch_id}</com.ctg:ServicingBranchId>
				</com.fnf.fundstransferadd_V2_0:XferAddRq>
			</BankSvcRq>
		</IFX>"""
      	
      	def IB04AutoBillRegInqRq = """<IFX xmlns="http://www.ifxforum.org/IFX_150">
			<SignonRq>
				<SessKey>${session_key}</SessKey>
				<ClientDt>${client_datetime}.000000-00:00</ClientDt>
				<CustLangPref>en_US</CustLangPref>
				<ClientApp>
					<Org>Internet Banking</Org>
					<Name>IB</Name>
					<Version>1.0</Version>
				</ClientApp>
			</SignonRq>
			<BankSvcRq>
				<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</RqUID>
				<AsyncRqUID>00000000-0000-0000-0000-000000000000</AsyncRqUID>
				<SPName>com.fnf.xes.PRF</SPName>
				<ns6:AutoBillRegInqRq xmlns:ns6="http://www.fnf.com/xes/ctg/services/autobillreg/autobillreginq/v1_0" xmlns:ns1="http://www.fnf.com/xes/ctg" xmlns:ns2="http://www.fnf.com/xes">
					<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</RqUID>
					<AsyncRqUID>623e7c95-8669-4fa0-87cd-2cfafe23b475</AsyncRqUID>
					<RefInfo>
						<RefType></RefType>
						<RefId></RefId>
					</RefInfo>
					<CustPermId>${cust_nbr}</CustPermId>
				</ns6:AutoBillRegInqRq>
			</BankSvcRq>
		</IFX>"""
      	
	val httpProtocol = http
		.baseURL("http://10.0.50.21:9083")
		.inferHtmlResources()
		.acceptEncodingHeader("gzip,deflate")
		.contentTypeHeader("text/xml")
		.userAgentHeader("Apache-HttpClient/4.1.1 (java 1.5)")

    	val uri1 = "10.0.50.21"
    	
       	val IB01 = scenario("IB01")
    		.feed(_ib01_acctinqrq_data_feeder)
    		.feed(_ib01_acctpromorq_data_feeder)
    		.feed(_ib01_custrq_data_feeder)
    		.feed(_ib01_inboxalertrq_data_feeder)
    		.feed(_client_datetime)
    		.feed(_session_key)
    		
    		.during(90 minutes) 
		{ 
		   exec(
		   	http("IB01_AcctInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01AcctInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB01_AcctPromoInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01AcctPromoInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB01_CustInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01CustInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB01_InboxAlertInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01InboxAlertInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
		}

       	val IB02 = scenario("IB02")
    		.feed(_ib01_custrq_data_feeder)
    		.feed(_ib01_inboxalertrq_data_feeder)
    		.feed(_client_datetime)
    		.feed(_session_key)
    		.feed(_ib02_feerq_data_feeder)
    		.feed(_ib02_pmtacct1_data_feeder)
    		.feed(_ib02_pmtacct2_data_feeder)

    		.during(90 minutes) 
		{ 
		    exec(
			http("IB02_CustInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01CustInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB02_InboxAlertInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01InboxAlertInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB02_FeeInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB02FeeInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)

		   .exec(
			http("IB02_PmtAddRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB02PmtAddRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)			
		}

      
       	val IB03 = scenario("IB03")
    		.feed(_ib01_custrq_data_feeder)
    		.feed(_ib01_inboxalertrq_data_feeder)
    		.feed(_ib01_acctinqrq_data_feeder)
		.feed(_ib01_acctpromorq_data_feeder)

    		.feed(_client_datetime)
    		.feed(_session_key)
    		.feed(_ib02_feerq_data_feeder)
    		.feed(_ib02_pmtacct1_data_feeder)
    		.feed(_ib02_pmtacct2_data_feeder)
    		
    		.feed(_ib03_balinq_data_feeder)
    		.feed(_ib03_xferadd_data_feeder)

    		.during(90 minutes) 
		{ 
		    exec(
			http("IB03_CustInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01CustInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB03_InboxAlertInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01InboxAlertInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
		
		   .exec(
		   	http("IB03_AcctInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01AcctInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB03_AcctPromoInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01AcctPromoInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB03_FeeInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB02FeeInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB03_BalInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB03BalInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)	
			
		   .exec(
			http("IB03_XferAddRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB03XferAddRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)			
		}

  	val IB04 = scenario("IB04")
    		.feed(_ib01_custrq_data_feeder)
    		.feed(_ib01_inboxalertrq_data_feeder)
    		.feed(_client_datetime)
    		.feed(_session_key)
    		.feed(_ib02_feerq_data_feeder)
    		.feed(_ib02_pmtacct1_data_feeder)
    		.feed(_ib02_pmtacct2_data_feeder)
    		.feed(_ib03_balinq_data_feeder)
		.feed(_ib03_AutoBillRegInqRq_data_feeder)
		
    		.during(90 minutes) 
		{ 
		    exec(
			http("IB04_CustInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01CustInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB04_InboxAlertInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB01InboxAlertInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
		
		   .exec(
			http("IB04_FeeInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB02FeeInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)

		   .exec(
			http("IB04_PmtAddRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB02PmtAddRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)
			
		   .exec(
			http("IB04_BalInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB03BalInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)	
			
		   .exec(
			http("IB04_AutoBillRegInqRq")
			.post("/xpress/services/ifx15/raw")
			.body(StringBody(IB04AutoBillRegInqRq))
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(20 seconds)			
		}
			
	setUp(
		  IB01.inject(
		    nothingFor(1 seconds),
		    atOnceUsers(5),
	    	    rampUsers(1300) over(30 minutes)    
		  ).protocols(httpProtocol),
	
		  IB02.inject(
		   nothingFor(1 seconds),
		   atOnceUsers(5),
		   rampUsers(110) over(30 minutes)    
		  ).protocols(httpProtocol),
		  
		  IB03.inject(
		   nothingFor(1 seconds),
		   atOnceUsers(5),
		   rampUsers(650) over(30 minutes)    
		  ).protocols(httpProtocol),

		  IB04.inject(
		   nothingFor(1 seconds),
		   atOnceUsers(5),
		   rampUsers(110) over(30 minutes)    
		  ).protocols(httpProtocol)
  	)
}


