var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "116033",
        "ok": "116030",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "12",
        "ok": "12",
        "ko": "248"
    },
    "maxResponseTime": {
        "total": "3543",
        "ok": "3543",
        "ko": "312"
    },
    "meanResponseTime": {
        "total": "351",
        "ok": "351",
        "ko": "274"
    },
    "standardDeviation": {
        "total": "232",
        "ok": "232",
        "ko": "27"
    },
    "percentiles1": {
        "total": "280",
        "ok": "280",
        "ko": "262"
    },
    "percentiles2": {
        "total": "434",
        "ok": "434",
        "ko": "287"
    },
    "percentiles3": {
        "total": "826",
        "ok": "826",
        "ko": "307"
    },
    "percentiles4": {
        "total": "1117",
        "ok": "1117",
        "ko": "311"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 109322,
        "percentage": 94
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 5902,
        "percentage": 5
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 806,
        "percentage": 1
    },
    "group4": {
        "name": "failed",
        "count": 3,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "25.796",
        "ok": "25.795",
        "ko": "0.001"
    }
},
contents: {
"req_atminternalft-f723e": {
        type: "REQUEST",
        name: "ATMInternalFT",
path: "ATMInternalFT",
pathFormatted: "req_atminternalft-f723e",
stats: {
    "name": "ATMInternalFT",
    "numberOfRequests": {
        "total": "9208",
        "ok": "9205",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "12",
        "ok": "12",
        "ko": "248"
    },
    "maxResponseTime": {
        "total": "3543",
        "ok": "3543",
        "ko": "312"
    },
    "meanResponseTime": {
        "total": "281",
        "ok": "281",
        "ko": "274"
    },
    "standardDeviation": {
        "total": "145",
        "ok": "145",
        "ko": "27"
    },
    "percentiles1": {
        "total": "261",
        "ok": "261",
        "ko": "262"
    },
    "percentiles2": {
        "total": "312",
        "ok": "312",
        "ko": "287"
    },
    "percentiles3": {
        "total": "484",
        "ok": "484",
        "ko": "307"
    },
    "percentiles4": {
        "total": "783",
        "ok": "783",
        "ko": "311"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 9119,
        "percentage": 99
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 62,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 24,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 3,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "2.047",
        "ok": "2.046",
        "ko": "0.001"
    }
}
    },"req_atmcashwithdraw-21b18": {
        type: "REQUEST",
        name: "ATMCashWithdrawal",
path: "ATMCashWithdrawal",
pathFormatted: "req_atmcashwithdraw-21b18",
stats: {
    "name": "ATMCashWithdrawal",
    "numberOfRequests": {
        "total": "90835",
        "ok": "90835",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17",
        "ok": "17",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3536",
        "ok": "3536",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "365",
        "ok": "365",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "percentiles1": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "percentiles2": {
        "total": "466",
        "ok": "466",
        "ko": "-"
    },
    "percentiles3": {
        "total": "848",
        "ok": "848",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1145",
        "ok": "1145",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 84763,
        "percentage": 93
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 5364,
        "percentage": 6
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 708,
        "percentage": 1
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "20.194",
        "ok": "20.194",
        "ko": "-"
    }
}
    },"req_atmbalinq-34d00": {
        type: "REQUEST",
        name: "ATMBalInq",
path: "ATMBalInq",
pathFormatted: "req_atmbalinq-34d00",
stats: {
    "name": "ATMBalInq",
    "numberOfRequests": {
        "total": "15990",
        "ok": "15990",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14",
        "ok": "14",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3530",
        "ok": "3530",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "316",
        "ok": "316",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "percentiles1": {
        "total": "251",
        "ok": "251",
        "ko": "-"
    },
    "percentiles2": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "percentiles3": {
        "total": "736",
        "ok": "735",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1015",
        "ok": "1015",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 15440,
        "percentage": 97
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 476,
        "percentage": 3
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 74,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "3.555",
        "ok": "3.555",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
