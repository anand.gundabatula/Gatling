var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "645609",
        "ok": "640257",
        "ko": "5352"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "15"
    },
    "maxResponseTime": {
        "total": "56655",
        "ok": "56655",
        "ko": "42787"
    },
    "meanResponseTime": {
        "total": "37228",
        "ok": "37365",
        "ko": "20844"
    },
    "standardDeviation": {
        "total": "40975",
        "ok": "41230",
        "ko": "16740"
    },
    "percentiles1": {
        "total": "46510",
        "ok": "46554",
        "ko": "27578"
    },
    "percentiles2": {
        "total": "49568",
        "ok": "49584",
        "ko": "36852"
    },
    "percentiles3": {
        "total": "52471",
        "ok": "52480",
        "ko": "39547"
    },
    "percentiles4": {
        "total": "54008",
        "ok": "54014",
        "ko": "41138"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 35007,
        "percentage": 5
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 8437,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 596813,
        "percentage": 92
    },
    "group4": {
        "name": "failed",
        "count": 5352,
        "percentage": 1
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "71.708",
        "ok": "71.114",
        "ko": "0.594"
    }
},
contents: {
"req_atminternalft-f723e": {
        type: "REQUEST",
        name: "ATMInternalFT",
path: "ATMInternalFT",
pathFormatted: "req_atminternalft-f723e",
stats: {
    "name": "ATMInternalFT",
    "numberOfRequests": {
        "total": "52109",
        "ok": "46757",
        "ko": "5352"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "15"
    },
    "maxResponseTime": {
        "total": "43071",
        "ok": "43071",
        "ko": "42787"
    },
    "meanResponseTime": {
        "total": "26540",
        "ok": "27192",
        "ko": "20844"
    },
    "standardDeviation": {
        "total": "15168",
        "ok": "14839",
        "ko": "16740"
    },
    "percentiles1": {
        "total": "35235",
        "ok": "35393",
        "ko": "27574"
    },
    "percentiles2": {
        "total": "37762",
        "ok": "37831",
        "ko": "36853"
    },
    "percentiles3": {
        "total": "40179",
        "ok": "40239",
        "ko": "39546"
    },
    "percentiles4": {
        "total": "41667",
        "ok": "41709",
        "ko": "41138"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 6381,
        "percentage": 12
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 212,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 40164,
        "percentage": 77
    },
    "group4": {
        "name": "failed",
        "count": 5352,
        "percentage": 10
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "5.788",
        "ok": "5.193",
        "ko": "0.594"
    }
}
    },"req_atmcashwithdraw-21b18": {
        type: "REQUEST",
        name: "ATMCashWithdrawal",
path: "ATMCashWithdrawal",
pathFormatted: "req_atmcashwithdraw-21b18",
stats: {
    "name": "ATMCashWithdrawal",
    "numberOfRequests": {
        "total": "507929",
        "ok": "507929",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56655",
        "ok": "56655",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38210",
        "ok": "38210",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "41617",
        "ok": "41617",
        "ko": "-"
    },
    "percentiles1": {
        "total": "47014",
        "ok": "47018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49760",
        "ok": "49761",
        "ko": "-"
    },
    "percentiles3": {
        "total": "52593",
        "ok": "52593",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54075",
        "ok": "54074",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 23964,
        "percentage": 5
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 7203,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 476762,
        "percentage": 94
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "56.416",
        "ok": "56.416",
        "ko": "-"
    }
}
    },"req_atmbalinq-34d00": {
        type: "REQUEST",
        name: "ATMBalInq",
path: "ATMBalInq",
pathFormatted: "req_atmbalinq-34d00",
stats: {
    "name": "ATMBalInq",
    "numberOfRequests": {
        "total": "85571",
        "ok": "85571",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14",
        "ok": "14",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56504",
        "ok": "56504",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37911",
        "ok": "37911",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "53170",
        "ok": "53170",
        "ko": "-"
    },
    "percentiles1": {
        "total": "46892",
        "ok": "46893",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49664",
        "ok": "49664",
        "ko": "-"
    },
    "percentiles3": {
        "total": "52491",
        "ok": "52491",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53973",
        "ok": "53973",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 4662,
        "percentage": 5
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 1022,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 79887,
        "percentage": 93
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "9.504",
        "ok": "9.504",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
