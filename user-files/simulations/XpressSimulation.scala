
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class XpressSimulation extends Simulation {

      val _data01_feeder = csv("CustInqRq.csv").circular
      
      def postrequest = """<IFX xmlns="http://www.ifxforum.org/IFX_150"
      	xmlns:com.fnf.custinq_V2_0="http://www.fnf.com/xes/services/cust/custinq/v2_0"
      	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
      	xmlns:com.fnf.partyacctrelinq.v2_0="http://www.fnf.com/xes/services/partyacctrel/partyacctrelinq/v2_0"
      	xmlns:com.fnf="http://www.fnf.com/xes" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      	<SignonRq>
      		<SignonPswd>
      			<CustId>
      				<SPName />
      				<CustLoginId>tibco</CustLoginId>
      			</CustId>
      			<CustPswd>
      				<CryptType />
      				<Pswd>TIBCO</Pswd>
      			</CustPswd>
      		</SignonPswd>
      		<ClientDt>2004-10-30T12:00:00.000000-00:00</ClientDt>
      		<CustLangPref>en_US</CustLangPref>
      		<ClientApp>
      			<Org>com.fnf</Org>
      			<Name>FNF Bank</Name>
      			<Version>1.0</Version>
      		</ClientApp>
      	</SignonRq>
      	<BaseSvcRq Id="ID000001">
      		<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b473</RqUID>
      		<SPName>com.fnf.xes.PRF</SPName>
      		<com.fnf.custinq_V2_0:CustInqRq>
      			<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b473</RqUID>
      			<CustId>
      				<SPName>com.fnf.xes.PRF</SPName>
      				<CustPermId>${cust_nbr}</CustPermId>
      				<CustLoginId>String</CustLoginId>
      			</CustId>
      		</com.fnf.custinq_V2_0:CustInqRq>
      		<com.fnf.partyacctrelinq.v2_0:PartyAcctRelInqRq>
      			<RqUID>623e7c95-8669-4fa0-87cd-2cfafe23b473</RqUID>
      			<CustId>
      				<SPName>com.fnf.xes.PRF</SPName>
      				<CustPermId>${cust_nbr}</CustPermId>
      			</CustId>
      			<RecCtrlIn>
      				<MaxRec>100</MaxRec>
      			</RecCtrlIn>
      		</com.fnf.partyacctrelinq.v2_0:PartyAcctRelInqRq>
      	</BaseSvcRq>
      </IFX>"""
      

	val httpProtocol = http
		.baseURL("http://10.0.50.21:9080")
		.inferHtmlResources()
		.acceptEncodingHeader("gzip,deflate")
		.contentTypeHeader("text/xml")
		.userAgentHeader("Apache-HttpClient/4.1.1 (java 1.5)")

    	val uri1 = "10.0.50.21"
    	val scn = scenario("XpressSimulation")
    		.feed(_data01_feeder)
		.exec(http("request_0")
			.post("/xpress/services/ifx15/raw")
			//.body(RawFileBody("+fs+")))
			.body(StringBody(postrequest)))
			//.body(RawFileBody("XpressSimulation_0000_request.txt")))
			
			
	setUp(scn.inject(atOnceUsers(3))).protocols(httpProtocol)
	
	//setUp(
	//	  scn.inject(
	//	    nothingFor(1 seconds),
	//	    atOnceUsers(1),
	//    	    rampUsers(1) over(1 minutes)    
	//	  ).protocols(httpProtocol)
  	//)
}


