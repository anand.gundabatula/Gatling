import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class JLTSimulation extends Simulation {

	val httpProtocol = http
		.baseURL("http://10.0.48.223:8123")
		.inferHtmlResources()
		.acceptHeader("text/html, application/xhtml+xml, */*")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-US")
		.doNotTrackHeader("1")
		.userAgentHeader("Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)")

	val _data02_feeder=csv("bp02.csv").circular
	
	val bp02scn = scenario("ATMCashWithdrawal")
		.feed(_data02_feeder)
		.during(120 minutes) 
		{ 
		   exec(
		   	http("ATMCashWithdrawal")
			.get("/bp?bpid=VTB_ATMCashWithdrawal&Account=${acct_num}&CurDate=0206")
			.check(regex("SUCCESS").exists)
			.check(regex("FAILED").notExists)
			).pause(10 seconds)
		}

	val _data01_feeder = csv("bp01.csv").circular
	
	val bp01scn = scenario("ATMBalInq")
		.feed(_data01_feeder)
		.during(120 minutes) 
		{ 
		   exec(
		   	http("ATMBalInq")
			.get("/bp?bpid=VTB_ATMBalInq&Account=${acct_num}&CurDate=0206")
			.check(regex("FAILED").notExists)
			).pause(10 seconds)
		}

	val _data03_feeder = csv("bp03.csv").circular
	val bp03scn = scenario("ATMInternalFT")
		.feed(_data03_feeder)
		.during(120 minutes) 
		{ 
		   exec(
		   	http("ATMInternalFT")
			.get("/bp?bpid=VTB_ATMInternalFT&CurDate=0206&BRCD=${branchcode}&Account=${fromAcct}&OtherAccount=${toAcct}")
			.check(regex("FAILED").notExists)
			).pause(10 seconds)
		}


	setUp(
	  bp01scn.inject(
	    nothingFor(1 seconds),
	    atOnceUsers(10),
	    rampUsers(558) over(30 minutes) //558	    
	   ).protocols(httpProtocol),
	    
	  bp02scn.inject(
	    nothingFor(1 seconds),
	    atOnceUsers(10),
    	    rampUsers(3383) over(30 minutes) //3383    
	  ).protocols(httpProtocol),

	  bp03scn.inject(
	    nothingFor(1 seconds),
	    atOnceUsers(10),
    	    rampUsers(254) over(30 minutes) //254    
	  ).protocols(httpProtocol)
  	)
}

