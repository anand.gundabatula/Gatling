
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class RecordedSimulation extends Simulation {

	val httpProtocol = http
		.baseURL("http://10.0.50.21:9080")
		.inferHtmlResources()
		.acceptEncodingHeader("gzip,deflate")
		.contentTypeHeader("text/xml")
		.userAgentHeader("Apache-HttpClient/4.1.1 (java 1.5)")



    val uri1 = "10.0.50.21"

	val scn = scenario("RecordedSimulation")
		.exec(http("request_0")
			.post("/xpress/services/ifx15/raw")
			.body(RawFileBody("RecordedSimulation_0000_request.txt")))

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}